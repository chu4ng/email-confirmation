package main

import (
    "log"
	"errors"
	"time"
    mail "github.com/xhit/go-simple-mail/v2"
    "github.com/google/uuid"

	"github.com/gofiber/fiber/v2"
)

var (
    UUIDTimeToLive = 10 * time.Minute
    hostAdress = "127.0.0.1:8080"
    SMTPAddress = "localhost"
    SMTPPort = 25
)

type User struct {
    Name string `json:"name"`
    Pass string `json:"pass"`
    Email string `json:"email"`
    id string
    confirmed bool
}

/*
    in one main service we create account with field Confirm: False (or store it in cache?) and call auth.EmailConfirm(email) from another service
    which have N timeout is out, delete account
    and it shall have callback?
    So, we create UUID-link, send it to email. ... Create new handler?
    if it visited, activate account somehow...
*/

type DB struct {
    Conn int
    users map[string]*User
    awaitsConfirm map[string]string
}

var db *DB

func (db *DB) AddUser(u *User) error {
    if _, ok := db.users[u.Name]; ok {
        return errors.New("user alredy exists")
    }
    db.users[u.id] = u

    return nil
}


func (db *DB) RemoveUser(uid string) {
    // place to do somwthing else
    delete(db.users, uid)
}

func (db *DB) GetUserByUUID(uuid string) (*User, error ) {
    for id, l_uuid := range db.awaitsConfirm {
        if l_uuid == uuid {
            return db.users[id], nil
        }
    }
    return nil, errors.New("no uuid request have been found")
}

// generate UUID, put it alongside with user.Id
func (db *DB) NewUUID(userId string) error {
    var user *User
    var err error
    uuid := uuid.New().String()

    if user, err = db.GetUserById(userId); err != nil {
        return err
    }

    if _, ok := db.awaitsConfirm[user.id]; ok {
        return errors.New("user is alredy waiting for confirmation")
    }

    db.awaitsConfirm[user.id] = uuid

    return nil
}

func (db *DB) GetUserById(userId string) (*User, error) {
    var err error = nil
    var user *User
    user, ok := db.users[userId]
    if !ok {
        err = errors.New("can't find user by id")
    }
    return user, err

}


// Add decorator to these handlers, so module's user can put his own handler(response).


func main() {
    db = &DB{
        users: make(map[string]*User),
        awaitsConfirm: make(map[string]string),
    }
        app := fiber.New()

        app.Post("/register", NewUser)
        app.Get("/confirm/:uuid", ActivateByUUID)

        log.Fatal(app.Listen(hostAdress))
    }

// handler/user.go

func  NewUser(c *fiber.Ctx) error {
    user := new(User)
    if err := c.BodyParser(user); err != nil{
        return c.SendString("can't create user" + err.Error())
    }
    user.id = "123"
    user.confirmed = false
    _ = user.Pass

    log.Printf("User: %+v\n", user)
    // check if name is already  exist

    if err := db.AddUser(user); err != nil {
        return c.SendString(err.Error())
    }
    if err := CreateUUIDTicket(user); err != nil {
        return c.SendString(err.Error())
    }

    return c.SendString("Successful registration. Await confirmation email")
}

func CreateUUIDTicket(user *User) error {
    if err := db.NewUUID(user.id); err != nil {
        return err
    }
    uuid := db.awaitsConfirm[user.id]

    if err := sendConfirmationEmail(user, uuid); err != nil {
        return err
    }
    go func(uuid string) {
    // delete user info if account haven't been activated during set timelimit
    // TODO also add time.Time expriration field in UUID-userId struct,
    // and make cleaning during db initialisation, in case programm was interrupted during this timeout.gg
            time.Sleep(UUIDTimeToLive)
            db.RemoveUser(uer.id)
        }(uuid)

    return nil
    }

func ActivateByUUID(c *fiber.Ctx) error {
        uuid := c.Params("uuid")
        user, err := db.GetUserByUUID(uuid) // TODO handle err
        if err !=  nil {
            nerr := errors.Join(errors.New("uuid activation handler"), err)
            return c.SendString(nerr.Error())
        }

        db.users[user.id].confirmed = true

        delete(db.awaitsConfirm, user.id)

        log.Printf("Account %s | %s has been successfuly activated", user.Name, user.Email)
        return c.SendString("Successful email confirmation")
    }

func sendConfirmationEmail(user* User, uuid string) error {
    server := mail.NewSMTPClient()

    // SMTP Server
    // setting for test purpose
    server.Host = SMTPAddress
    server.Port = SMTPPort
    server.Username = "test"
    server.Password = ""
    server.Encryption = mail.EncryptionNone

    smtpClient,err := server.Connect()
    if err != nil{
        err = errors.Join(errors.New("can't send confirmation email"), err)
        return err
	}

    email := mail.NewMSG()
	email.SetFrom("From Test <admin@" + SMTPAddress + ">").
		AddTo(user.Email).
		SetSubject("Account activation")


	msg := []byte("Click on link to confirm:\r\n" +
        "http://" + hostAdress + "/confirm/" + uuid + "\r\n") // TODO replace with actual address

    email.SetBodyData(mail.TextHTML, msg)

    if err := email.Send(smtpClient); err != nil {
        err = errors.Join(errors.New("can't send confirmation email"), err)
        return err
    }

    return nil
}
